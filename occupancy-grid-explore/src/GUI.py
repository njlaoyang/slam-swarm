"""
A basic GUI for the entire system
"""
from tkinter import *

class GUI:
	def __init__(self):
		master = Tk()
		self.canvas = Canvas(master, width = 600, height = 600)
		self.width, self.height = 600, 600
		self.canvas.pack()

	def __init__(self, width, height):
		master = Tk()
		self.width, self.height = width, height
		self.canvas = Canvas(master, width = self.width, height = self.height)
		self.canvas.pack()

	def renderCoordinate(self, i, j, grid):
		per_x, per_y = self.height / grid.height, self.width / grid.width

		if grid.isUnknown(i, j):
			self.canvas.create_rectangle(i * per_x, j * per_y, (i + 1) * per_x, (j + 1) * per_y, fill = "#808080")
		elif grid.isFree(i, j):
			self.canvas.create_rectangle(i * per_x, j * per_y, (i + 1) * per_x, (j + 1) * per_y, fill = "white")
		else:
			self.canvas.create_rectangle(i * per_x, j * per_y, (i + 1) * per_x, (j + 1) * per_y, fill = "black")


	"""
	Render the entire grid. (Supposedly slow, should only be called once at the beginning)
	"""
	def renderGrid(self, grid):
		for i in range(grid.height):
			for j in range(grid.width):
				self.renderCoordinate(i, j, grid)

	"""
	Update each grid's color based on mBot's perceptions
	"""
	def updateGrid(self, mBot):
		for bot in mBot.picketBots:
			self.renderCoordinate(int(bot.x), int(bot.y), mBot.perception)

	"""
	Render masterBot and picketBots on the grid as red and blue circles
	"""
	def renderRobots(self, mBot):
		grid = mBot.perception
		per_x, per_y = self.height / grid.height, self.width / grid.width
		self.canvas.create_oval((mBot.x + 0.25) * per_x, (mBot.y + 0.25) * per_y, (mBot.x + 0.75) * per_x , (mBot.y + 0.75) * per_y,fill = "red")

		for bot in mBot.picketBots:
			self.canvas.create_oval((bot.x + 0.25) * per_x, (bot.y + 0.25) * per_y, (bot.x + 0.75) * per_x , (bot.y + 0.75) * per_y,fill = "blue")

	"""
	Delete all robot circles on the grid
	"""
	def removeRobots(self, mBot):
		positions = [(bot.x, bot.y) for bot in mBot.picketBots]
		positions.append((mBot.x, mBot.y))
		for i, j in positions:
			self.renderCoordinate(i, j, mBot.perception)