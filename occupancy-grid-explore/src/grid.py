"""
The grid class represents the True grid in which the robots live.
"""

class grid:
	height, width = 0, 0
	data = []

	"""
	Initialize a default grid;
	"""
	def __init__(self):
		self.height = 5
		self.width = 5
		self.data = [0, 0, 0, 0, 0, 
					 0, 1, 1, 1, 0, 
					 0, 1, 0, 1, 0,
					 0, 1, 1, 1, 0,
					 0, 0, 0, 0, 0
					]

	"""
	Initiallize a grid with height m, width n and the data as provided
	The data should be expressed in row-priority order.
	"""
	def __init__(self, m, n, data):
		self.height = m
		self.width = n
		self.data = data

	"""
	Return whether a coordinate (x, y) contains an obstacle.
	x, y are 0-indexed
	"""
	def isFree(self, x, y):
		x, y = int(x), int(y)

		pos = x * self.width + y
		if (x < 0) or (x >= self.height):
			return False
		if (y < 0) or (y >= self.width):
			return False
		return (self.data[pos] == 0)

	"""
	Returns whether the situation of a coordinate (x, y) is is Unknown
	For use of masterBot's perception only.
	"""
	def isUnknown(self, x, y):
		x, y = int(x), int(y)
		pos = x * self.width + y
		if (x < 0) or (x >= self.height):
			return False
		if (y < 0) or (y >= self.width):
			return False
		return (self.data[pos] == 2)

	"""
	For use in masterBot perception only
	updates the perception of a certain coordinate
	"""
	def update(self, x, y, val):
		x, y = int(x), int(y)
		if (x < 0) or (x >= self.height) or (y < 0) or (y >= self.width):
			print("Error: index out of bounds")
			return
		pos = x * self.width + y
		self.data[pos] = val

	"""
	Returns whether a coordinate (x, y) is valid in the grid.
	"""
	def isValid(self, x, y):
		x, y = int(x), int(y)
		if (x < 0) or (x >= self.height):
			return False
		if (y < 0) or (y >= self.width):
			return False
		return True

	def __repr__(self):
		ans = ""
		count = 0
		for i in range(self.height):
			for j in range(self.width):
				ans = ans + " " + str(self.data[count])
				count += 1
			ans += "\n"
		return ans
