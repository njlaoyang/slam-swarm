from GUI import GUI

class hybridGUI(GUI):
	"""
	Render masterBot and picketBots on the grid as red and blue circles
	"""
	def renderRobots(self, mBot):
		grid = mBot.perception
		per_x, per_y = self.height / grid.height, self.width / grid.width
		self.canvas.create_oval((mBot.x - 0.25) * per_x, (mBot.y - 0.25) * per_y, (mBot.x + 0.25) * per_x , (mBot.y + 0.25) * per_y,fill = "red")

		for bot in mBot.picketBots:
			self.canvas.create_oval((bot.x - 0.25) * per_x, (bot.y - 0.25) * per_y, (bot.x + 0.25) * per_x , (bot.y + 0.25) * per_y,fill = "blue")
	

	def removeRobots(self, mBot):
		positions = [(int(bot.x), int(bot.y)) for bot in mBot.picketBots]
		positions.append((int(mBot.x), int(mBot.y)))
		per_x, per_y = self.height / mBot.perception.height, self.width / mBot.perception.width
		for i, j in positions:
			for di in [-1, 0, 1]:
				for dj in [-1, 0, 1]:
					self.renderCoordinate(i + di, j + dj, mBot.perception)
