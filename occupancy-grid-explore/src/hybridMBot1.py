"""
Implementation of "hybrid robot 1":
Change #1: Robots are no longer grid-aligned, and moves according to speed and angle. 
Change #2: MasterBot can only see robots within a certain angle and distance
Change #3: picketBot moves with uncertainty: it follows instruction with probability 0.7
and chooses a uniformly random direction/velocity pair with probability 0.3.

Algorithm detail: (Based on my speculation)
First, the masterBot designates a sector (pizza-slice) for picketBots to explore, and 
assigns each picketBot to explore a smaller "pizza slice" (divided by angles).
Each picketBot then explores the area in a "sweeping" fashion. Each robot first covers
area with radius 1, then radius 2, etc... If any picketBot ever moves out of its designated
region, the next command will be asking him to go back to where it should be.
Then masterBot moves and selects a new pizza-slice for picketBots to explore, it moves
in the following manner:
1) It moves (supposedly) according to a boundary line of the pizza wedge
2) It turns to further explore the region.

"""
from math import *
from masterBot import masterBot
from noisypBot import noisypBot
import random


"""
Revise an angle so that  it's between 0 and 2*pi
"""
def reviseAngle(x):
	while (x <= 0):
		x += 2 * pi
	while (x > 2 * pi):
		x -= 2 * pi
	return x

"""
Returns whether angle x is between angle y and angle z
First need to shift every angle so that they are between 0 and 2*pi
"""
def inRange(x, y, z, direction):
	x, y, z = reviseAngle(x), reviseAngle(y), reviseAngle(z)

	if (y < z):
		if (direction == 1):
			return (x <= z) and (x >= z - pi)
		else:
			return (x >= y) and  (x <= y + pi)
			#return (x >= y) and (x <= z)
	else:
		if (direction == 1):
			return (x <= z) or (x >= z + pi) # Stupid design
		else:
			return (x >= y) or (x <= y - pi)

class hybridMBot1(masterBot):
	"""
	Initializer
	"""
	def __init__(self, x, y, ngrid, masterSpeed, numPicketBots, picketBotSpeed, picketBotAccuracy): 
		self.angle = 7 * pi / 4
		self.visualAngle = pi / 4 # It sees with an angle of 90 degress
		self.visualRange = 8 # It sees 8 pixels at a time
		self.status = "sweep"
		self.picketBotAccuracy = picketBotAccuracy
		super(hybridMBot1, self).__init__(x, y, ngrid, masterSpeed, numPicketBots, picketBotSpeed)

	"""
	Note: noisypBot is the class for picketBots in this case
	Since the algorithm starts in "sweep" mode, it first assigns all robots a proper 
	"angle range" with a "start angle" and an "end angle"
	Then it puts each robot at distance 1 from the masterBot's position w/ angle according
	to the robot's "start angle"
	"""
	def initializePicketBots(self, numPicketBots, picketBotSpeed):
		self.anglePerBot = self.visualAngle * 2.0 / numPicketBots;
		self.startAngle = [self.anglePerBot * i + self.angle - self.visualAngle for i in range(numPicketBots + 1)];
		print(self.startAngle)
		initial_radius = 1 #Subject to change
		self.picketBots = []
		self.expectedPos = [] #For now, if a robot deviates from path, just brute-forcely ask it to go back.
		self.currDir = [] #The current direction of the robot (increasing (1) /decreasing (-1) angles)
		self.justSwitched = [True for _ in range(numPicketBots)] # Stupid design, should be abandoned in the end
		for i in range(numPicketBots):
			ang = self.startAngle[i]
			x, y = self.x + initial_radius * cos(ang), self.y + initial_radius * sin(ang)
			#print(ang, atan2(initial_radius * sin(ang), initial_radius * cos(ang)))
			self.expectedPos.append((x, y))
			self.picketBots.append(noisypBot(x, y, self.grid, picketBotSpeed, self.picketBotAccuracy));
			self.currDir.append(1)

	"""
	For this model, a robot is within sight if and only if 
		1) Its distance from master robot is within visual visual range
		2) Its angle from master is also correct.
	"""
	def withinSight(self, x, y):
		dist = (x - self.x) * (x - self.x) + (y - self.y) * (y - self.y)
		angle = atan2((y - self.y) , (x - self.x))
		return (dist < self.visualRange * self.visualRange) and (angle > self.angle - self.visualAngle) and (angle < slef.angle + self.visualAngle)

	"""
	If a robot is in sweep mode, and hasn't finished exploring its own pizza slice yet, he:
	1) If he has finished with a certain radius, increase radius by 1 (i.e. move 1 unit "outer")
	2) If he hasn't finished a certain radius, move 1 unit perpendicular to the radius 
	   in a correct direction.
	"""
	def moveRobots(self):
		print("Current mode: " + self.status)
		if (self.status == "sweep"):
			all_finished = True
			for i in range(len(self.picketBots)):
				bot = self.picketBots[i]
				curr_radius = sqrt((bot.x - self.x) * (bot.x - self.x) + (bot.y - self.y) * (bot.y - self.y))
				angle = atan2((bot.y - self.y) , (bot.x - self.x))
				if (angle < 0):
					angle += 2 * pi

				if (curr_radius > self.visualRange - 0.001):
					pass #If the current robot has finished exploring, do nothing
				else:
					all_finished = False
					if (abs(bot.x - self.expectedPos[i][0]) < 0.00001) and (abs(bot.y - self.expectedPos[i][1]) < 0.00001):
						print(angle, " ", self.startAngle[i+1])
						print(angle, " ", self.startAngle[i])
						if (not self.justSwitched[i]) and (not inRange(angle, self.startAngle[i], self.startAngle[i + 1], self.currDir[i])):
							print("Switched")
							dx, dy = bot.speed * cos(angle - self.currDir[1] * 0.1), bot.speed * sin(angle - self.currDir[1] * 0.1)
							self.expectedPos[i] = [bot.x + dx, bot.y + dy]
							bot.moveTo(bot.x + dx, bot.y + dy)
							self.currDir[i] *= -1
							self.justSwitched[i] = True
						else:						
							self.justSwitched[i] = False
							dx, dy = bot.speed * cos(angle + pi / 2 * self.currDir[i]), bot.speed * sin(angle + pi / 2 * self.currDir[i])
							#print(dx, " ", dy)
							self.expectedPos[i] = [bot.x + dx, bot.y + dy]
							bot.moveTo(bot.x + dx, bot.y + dy)

					else:
						dx = self.expectedPos[i][0] - bot.x
						dy = self.expectedPos[i][1] - bot.y
						ratio = min(1, sqrt(dx * dx + dy * dy)/ bot.speed)
						bot.moveTo(bot.x + dx * ratio, bot.y + dy * ratio)

			if all_finished:
				self.status = "explore"
		else:
			theta = random.random() * 2.0 * pi
			dx, dy = self.visualRange * cos(theta), self.visualRange * sin(theta)
			while (not self.grid.isValid(self.x + dx, self.y + dy)):
				theta = random.random() * 2.0 * pi
				dx, dy = self.visualRange * cos(theta), self.visualRange * sin(theta)
			self.moveTo(self.x + dx, self.y + dy)
			self.angle += pi
			self.initializePicketBots(len(self.picketBots), self.picketBots[0].speed)
			self.status = "sweep"

	"""
	TODO: this should be deleted
	TODO: noisy bots
	"""
	def updatePicketBots(self):
		pass