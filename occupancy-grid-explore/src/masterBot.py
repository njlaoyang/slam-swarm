"""
The masterBot class extends the basic robot class
It represents the master robot in the slam process
"""

from robot import robot
from picketBot import picketBot
import random
from grid import grid


class masterBot(robot):
	picketBots = []
	perception = None
	grid = None
	"""
	masterBot has an extra parameter that stores the picketBots it is 
	currently mastering
	"""
	def __init__(self):
		super(masterBot, self).__init__()
		self.picketBots = []
		self.perception = None

	def __init__(self, x, y, ngrid, speed):
		super(masterBot, self).__init__(x, y, ngrid, speed)
		self.picketBots = []
		initial_perception = [2 for _ in range(ngrid.height * ngrid.width)]
		self.perception = grid(ngrid.height, ngrid.width, initial_perception)

	"""
	Extra constructor that stores picketBots being supervised
	"""
	def __init__(self, x, y, ngrid, masterSpeed, numPicketBots, picketBotSpeed):
		super(masterBot, self).__init__(x, y, ngrid, masterSpeed)
		#print(ngrid.height)

		self.initializePicketBots(numPicketBots, picketBotSpeed)
		initial_perception = [2 for _ in range(ngrid.height * ngrid.width)]
		self.perception = grid(ngrid.height, ngrid.width, initial_perception)

	"""
	Method used to initialize the position of picketBots
	Currently doing random initialization, as long as the picketBots are
	within sight of masterBot
	"""
	def initializePicketBots(self, numPicketBots, picketBotSpeed):
		for _ in range(numPicketBots):
			px, py = random.randint(0, self.grid.height - 1), random.randint(0, self.grid.width - 1)
			while (not self.withinSight(px, py)):
				px, py = random.randint(0, self.grid.height - 1), random.randint(0, self.grid.width - 1)
			self.picketBots.append(picketBot(px, py, self.grid, picketBotSpeed))

	"""
	Returns whether position (x, y) is within the sight of masterBot
	Currently returns true forever
	Subclasses should override this for more specific implementation
	"""
	def withinSight(self, x, y):
		return True

	"""
	The main method for masterBot's action
	"""
	def act(self):
		self.moveRobots()
		self.updatePicketBots()
		self.updatePerception()

	"""
	Move the masterBot and subsequent picketBots
	Subclasses should override this with different implementation
	Current implementation:
	Random movement of every robot.
	"""
	def moveRobots(self):
		for bot in self.picketBots:
			possiblePositions = bot.getMoveLocations(self.grid)
			dst = random.choice(possiblePositions)
			bot.moveTo(dst[0], dst[1])

		possiblePositions = self.getMoveLocations(self.grid)
		dst = random.choice(possiblePositions)
		self.moveTo(dst[0], dst[1])

	"""
	This method should be invoked after every robot move
	Removes all "out-of-sight" robots from ownership
	"""
	def updatePicketBots(self):
		remainingBots = []
		for bot in self.picketBots:
			if self.withinSight(bot.x, bot.y):
				remainingBots.append(bot)

		self.picketBots = remainingBots

	"""
	Update the masterbot's perception of the entire grid based on picketBot's
	perception
	"""
	def updatePerception(self):
		for bot in self.picketBots:
			self.perception.update(bot.x, bot.y, bot.getSensorReading(self.grid))