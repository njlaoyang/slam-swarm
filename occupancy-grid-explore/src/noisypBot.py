from picketBot import picketBot
import random
from math import *

"""
a noisypBot is just a pBot with following changes:
it's position is no longer grid-aligned
when it is asked to move to a certain position, it may or may not follows the command
"""
class noisypBot(picketBot):

	def __init__(self, x, y, ngrid, speed, accuracy):
		super(noisypBot, self).__init__(x, y, ngrid, speed)
		self.accuracy = accuracy

	"""
	A noisypBot moves with a noise.
	With probability equal to self.accuracy its move follows the instructions
	Otherwise it moves with a random speed and angle
	"""
	def moveTo(self, destX, destY):
		if random.random() < self.accuracy:
			self.x, self.y = destX, destY
		else:
			ang = random.random() * 2 * pi;
			mag = random.random() * self.speed;
			self.x += mag * cos(ang);
			self.y += mag * sin(ang);


