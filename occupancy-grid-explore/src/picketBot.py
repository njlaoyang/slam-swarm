from robot import robot
class picketBot(robot):
	"""
	Returns the robot's sensor reading (1 for obstacle and 0 for free)
	(for now, sensor reading is 100 percent accurate in accordance with the map)
	Should be overridden by subclasses
	"""
	def getSensorReading(self, grid):
		if grid.isFree(int(self.x), int(self.y)):
			return 0
		else:
			return 1
