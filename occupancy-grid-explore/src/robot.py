"""
The robot class simulates the behavior of a slave robot.
"""

class robot:
	x, y, speed = 0, 0, 0
	grid = None
	"""
	Default initializer, with position at (1, 1) and no grid
	"""
	def __init__(self):
		self.__init__(1, 1, None, 1)

	"""
	Initializer that specifies the robot's position, grid and speed
	"""
	def __init__(self, x, y, ngrid, speed):
		self.x, self.y = x, y
		self.grid = ngrid
		self.speed = speed

	"""
	Returns a possible locations that the robot can move to based on
	its speed limit.
	"""
	def getMoveLocations(self, grid):
		ans = []
		for dx in range(-(int(self.speed) + 1), (int(self.speed) + 1)):
			for dy in range(-(int(self.speed) + 1), (int(self.speed) + 1)):
				if ((dx * dx + dy * dy <= self.speed * self.speed) and grid.isValid(self.x + dx, self.y + dy)):
					ans.append((self.x + dx, self.y + dy))
		return ans

	"""
	Moves the robot to the new destination (destX, destY)
	This method follows instructions faithfully.
	Subclasses should override this method to allow noisy movements
	"""
	def moveTo(self, destX, destY):
		self.x, self.y = destX, destY
